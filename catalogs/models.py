from django.db import models
from django.utils.text import slugify


def generate_order_number(instance):
    if instance:
        klass = instance.__class__
        if klass.objects.exists():
            return klass.objects.count() + 1
    return 1


class Collection(models.Model):
    name = models.CharField(max_length=100)
    order_number = models.IntegerField("Numéro d'ordre")
    slug = models.SlugField(editable=False)

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        self.slug = slugify(self.name)
        if not self.order_number:
            self.order_number = generate_order_number(self)
        super().save(**kwargs)


class Category(models.Model):
    collection = models.ForeignKey(Collection, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=100)
    primary_category = models.BooleanField(default=False)
    slug = models.SlugField(editable=False)

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        self.slug = slugify(self.name)
        super().save(**kwargs)
