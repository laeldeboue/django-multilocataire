from ninja.schema import Schema
from ninja.router import Router
from catalogs.models import Category, Collection

router = Router(tags=['Catalogue'])


class CollectionOut(Schema):
    id: int
    name: str
    order_number: int
    slug: str


class CategoryOut(Schema):
    id: int
    collection: CollectionOut
    name: str
    primary_category: bool
    slug: str


class CollectionIn(Schema):
    name: str
    order_number: int = 1


class CategoryIn(Schema):
    collection: int = 1
    name: str
    primary_category: bool = False


@router.post('/create/collection/')
def collection_create(request, payload: CollectionIn):
    data = payload.dict()
    collection = Collection.objects.create(**data)
    return {
        'detail': f'La collection «{collection.name}» a été créée avec succès.',
        'collection': collection.name
    }


@router.post('/categoy/create/')
def category_create(request, payload: CategoryIn):
    data = payload.dict()
    try:
        collection = Collection.objects.get(id=data['collection'])
        del data['collection']
        category = Category.objects.create(
            collection=collection,
            **data
        )
        return {
            'detail': f"La catégorie «{category.name}» a été ajouté avec succès.",
            'success': True
        }
    except Collection.DoesNotExist:
        return {
            'detail': "La collection spécifiée n'existe pas.",
            'success': False
        }
