from django.contrib import admin

from .models import Category, Collection, generate_order_number


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'collection')


@admin.register(Collection)
class CollectionAdmin(admin.ModelAdmin):
    list_display = ('name',)

    # def auto_create_order_number(self, obj):
    #     if not obj.order_number:
    #         obj.order_number = generate_order_number(self)
    #         obj.save()
