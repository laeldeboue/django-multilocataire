from django.urls import path
from catalogs import views

app_name = "catalog"
urlpatterns = [
    path('', views.tenant_home, name="home-tenant")
]
