import httpx
from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse

from products.models import Product
from tenants.models import Domain


def get_domain(request):
    protocol = 'http://'
    base_url = request.get_host()
    # schema_name = path.split(':')[0]
    # domain = Domain.objects.get(domain=schema_name)
    return f"{protocol}{base_url}"


def product_list(request):
    # domain = get_domain(request)
    template_name = 'products/index.html'
    # print("URL", f"{domain}/api/products/1/")
    # endpoint = f"{domain}/api/products/1/"
    results = httpx.get('http://carshop.localhost:8000/api/products/1/')
    context = {
        "products": results.json(),
    }

    return render(request, template_name, context)