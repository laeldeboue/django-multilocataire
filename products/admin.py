from django.contrib import admin

from .models import ImageCatalog, Product


@admin.register(ImageCatalog)
class ImageCatalogAdmin(admin.ModelAdmin):
    list_display = ('name', 'product')


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'category')


