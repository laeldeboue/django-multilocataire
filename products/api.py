from decimal import Decimal

from ninja import Form
from ninja.router import Router
from ninja.schema import Schema

from typing import List

from catalogs.api import CategoryOut
from catalogs.models import Category
from products.models import Product

router = Router(tags=['Produits'])


class ProductIn(Schema):
    category: int
    name: str
    price: float = 0.0
    description: str = None


class ProductOut(Schema):
    uid: str
    category: CategoryOut
    name: str
    price: float
    slug: str
    description: str = None


@router.get('/{int:category_id}/', response=List[ProductOut])
def product_list(request, category_id: int):
    category = Category.objects.get(id=category_id)
    return Product.objects.filter(category=category)


@router.post('/create/')
def product_create(request, payload: ProductIn = Form(...)):
    data = payload.dict()
    try:
        category = Category.objects.get(id=data['category'])
        del data['category']
        product = Product.objects.create(
            category=category,
            **data
        )
        return {
            'detail': f"Le product {product.name} a été enregistré avec succès.",
            'success': True,
        }
    except Category.DoesNotExist:
        return {
            'detail': "Le produit spécifié n'existe pas.",
            'success': False,
        }


@router.get('/{str:item_uid}/', response=ProductOut)
def product_item(request, item_uid: str):
    product = Product.objects.get(uid=item_uid)
    return {
        'uid': product.uid,
        'category': product.category,
        'name': product.name,
        'price': product.price,
        'slug': product.slug,
        'desc': product.description,
    }
