from uuid import uuid4
from django.db import models
from django.utils.text import slugify

from catalogs.models import Category


def unique_id():
    return f'{uuid4()}'.replace('-', '')


class Product(models.Model):
    uid = models.CharField('ID', max_length=100, primary_key=True, default=unique_id)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='products')
    name = models.CharField(max_length=100)
    price = models.DecimalField(decimal_places=2, max_digits=10)
    slug = models.SlugField()
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        self.slug = slugify(self.name)
        super(Product, self).save(**kwargs)


class ImageCatalog(models.Model):
    name = models.CharField(max_length=50)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images')
    path = models.ImageField(upload_to='media/catalog_image/')

    def __str__(self):
        return self.name
