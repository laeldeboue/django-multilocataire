anyio==3.7.1
appdirs==1.4.4
appnope==0.1.3
asttokens==2.2.1
backcall==0.2.0
certifi==2023.7.22
comm==0.1.2
debugpy==1.6.4
decorator==5.1.1
distlib==0.3.2
entrypoints==0.4
exceptiongroup==1.1.3
executing==1.2.0
filelock==3.0.12
h11==0.14.0
httpcore==0.17.3
httpx==0.24.1
idna==3.4
ipykernel==6.19.4
ipython==8.7.0
jedi==0.18.2
jupyter_client==7.4.8
jupyter_core==5.1.1
matplotlib-inline==0.1.6
mysql-connector-python==8.0.31
nest-asyncio==1.5.6
packaging==22.0
parso==0.8.3
pexpect==4.8.0
pickleshare==0.7.5
platformdirs==2.6.0
prompt-toolkit==3.0.36
psutil==5.9.4
ptyprocess==0.7.0
pure-eval==0.2.2
Pygments==2.13.0
python-dateutil==2.8.2
pyzmq==24.0.1
six @ file:///AppleInternal/BuildRoot/Library/Caches/com.apple.xbs/Sources/python3/python3-103/six-1.15.0-py2.py3-none-any.whl
sniffio==1.3.0
stack-data==0.6.2
tornado==6.2
traitlets==5.8.0
virtualenv==20.4.7
wcwidth==0.2.5
