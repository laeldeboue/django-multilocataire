from setup.api import api
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('catalogs.urls', namespace='catalog')),
    path('products/', include('products.urls', namespace='product')),
    path('admin/', admin.site.urls),
    path('api/', api.urls),
]
