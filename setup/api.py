from ninja import NinjaAPI
from products.api import router as product_router
from catalogs.api import router as catalog_router

api = NinjaAPI()

api.add_router('/products/', product_router)
api.add_router('/catalogs/', catalog_router)